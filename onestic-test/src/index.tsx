import { Provider } from "mobx-react";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { App } from "./components/app/App";
import "./index.css";
import { rootModel } from "./model/rootStore";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(
  <Provider rootModel={rootModel}>
    <App />
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
