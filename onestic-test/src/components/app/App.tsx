import { inject, observer } from "mobx-react";
import * as React from "react";
import { IRootModel } from "../../model/rootStore";
import { Header } from "./header/Header";
import { Main } from "./main/Main";
import {ProductDialog} from './productDialog/ProductDialog';
import { RightPanel } from "./rightPanel/RightPanel";
import { appStyle } from "./style";

@inject("rootModel")
@observer
export class App extends React.Component<{ rootModel?: IRootModel }> {
  constructor(props: any) {
    super(props);
    this.props.rootModel!.catalog.getCatalogData();
  }
  
  public render() {
    return (
      <>
        <div className={appStyle}>
          <Header rootModel={this.props.rootModel} />
          <Main catalog={this.props.rootModel!.catalog} cart={this.props.rootModel!.cart} layout={this.props.rootModel!.layout}/>
          <RightPanel layout={this.props.rootModel!.layout} catalog={this.props.rootModel!.catalog}/>
          <ProductDialog layout={this.props.rootModel!.layout} cart={this.props.rootModel!.cart}/>
        </div>
      </>
    );
  }
}

export default App;
