import { AppBar, IconButton, Toolbar } from "@material-ui/core";
import UserIcon from "@material-ui/icons/Face";
import MenuIcon from "@material-ui/icons/Menu";
import ShoppingCart from "@material-ui/icons/ShoppingCart";
import * as React from "react";
import { OnesticLogo } from './OnesticLogo';
import { appBar, menuButton, shoppingCartButton, userButton } from "./style";

export interface IHeader {
  rootModel?: any;
}

export class Header extends React.Component<IHeader> {
  public render() {
    return (
      <AppBar className={appBar}>
        <Toolbar>
          <IconButton
            className={menuButton}
            color="inherit"
            aria-label="Menu"
            onClick={this.handleClick}
          >
            <MenuIcon />
          </IconButton>

          <OnesticLogo />
        
          <IconButton
            className={shoppingCartButton}
            color="inherit"
            aria-label="shopping cart"
          >
            <ShoppingCart />
          </IconButton>

          <IconButton
            className={userButton}
            color="inherit"
            aria-label="user"
          >
            <UserIcon />
          </IconButton>
          
        </Toolbar>
      </AppBar>
    );
  }

  private handleClick = () => {
    this.props.rootModel.layout.setSidePanelStatus(true);
  };
}
