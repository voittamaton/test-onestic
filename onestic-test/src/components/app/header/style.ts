import { media, style } from "typestyle/lib";

export const menuButton = style(media({
  minWidth: 960
},
{
  display: 'none'
}));

const panelWidth = 250; // left panel width. This breaks the rule of component independency but... I'll refactor
// it I have some time...

export const appBar = style({
    marginLeft: panelWidth, 
    position: 'absolute',
  }, 
  media({minWidth: 960}, {width: `calc(100% - ${panelWidth}px)`})
);

export const shoppingCartButton = style({
  float: 'right',
  position: 'relative'
});

export const userButton = style({
  float: 'right',
  position: 'relative'
});

export const titleStyle = style({
  flex: 1
});

export const onesticLogo = style({
  backgroundImage: `url(${process.env.PUBLIC_URL}'/logo_onestic.svg')`,
  backgroundRepeat: 'no-repeat',
  flexGrow: 1,
  height: 24,
  width: 100
});