import * as React from "react";
import { onesticLogo } from "./style";

export interface IHeader {
  rootModel?: any;
}

export class OnesticLogo extends React.Component<IHeader> {
  public render() {
    return (
      <div className={onesticLogo}/>
    );
  }
}
