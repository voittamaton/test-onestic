import { style } from "typestyle";
export const appStyle = style({
  display: "flex",
  flexGrow: 1,
  height: '100vh',
  overflow: "hidden",
  position: "relative",
  width: "100%",
  zIndex: 1
});
