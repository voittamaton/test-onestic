import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import { observer } from "mobx-react";
import * as React from "react";
import { ICart, ILayout } from "../../../model/rootStore";
import { addToCartButton, innerDialog } from './style';

export interface IProductDialog {
  layout: ILayout;
  cart: ICart
}

@observer
export class ProductDialog extends React.Component<IProductDialog> {
  public render() {
    return (
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={this.props.layout!.modalStatus}
        onClose={() => this.props.layout!.setModalStatus(false)}
      >
        <DialogTitle id="simple-dialog-title">{this.props.layout!.selectedItem.name}</DialogTitle>
        <div className={ innerDialog }>
          <div>
            <img src={this.props.layout!.selectedItem.image} alt={this.props.layout!.selectedItem.name} />
          </div>

          <div>
            <span>{this.props.layout!.selectedItem.categoryPath.map((category: any) => ` ⇒ ${category.name}`)}</span>
            {this.props.layout!.selectedItem.shortDescription}
          </div>
          <IconButton onClick={() => this.handleAddToCart(this.props.layout!.selectedItem)} className={addToCartButton}>
              <AddShoppingCartIcon />
            </IconButton>
        </div>
      </Dialog>
    );
  }

  private handleAddToCart = (item: any) => {
    this.props.cart!.addProduct(item);
  }
}
