import { style } from "typestyle/lib";

export const innerDialog = style({
  $nest: {
    div: {
        flex: '50%',
        paddingBottom: '10px'
    },
    img: {
        height: 200,
        marginRight: 20
    }
  },
  display: 'flex',
  padding: 20,
});

export const addToCartButton = style({
    bottom: '5px',
    position: 'absolute',
    right: '5px'
})
