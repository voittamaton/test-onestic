import { media, style } from "typestyle/lib";

export const mainDrawer = style(media({
    maxWidth: 960
}, {
    position: 'relative'
}))