import {
  Drawer,
  Hidden,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from "@material-ui/core";
import DateIcon from "@material-ui/icons/DateRange";
import CurrencyIcon from "@material-ui/icons/MonetizationOn";
import { observer } from "mobx-react";
import * as React from "react";
import { style } from "typestyle/lib";
import { ICatalog } from "../../../model/rootStore";
import { mainDrawer } from './style';

export interface IRightPanel {
  layout: any;
  catalog?: ICatalog;
}

@observer
export class RightPanel extends React.Component<IRightPanel> {
  public render() {
    const list = (
    <div
      className={style({
        width: 250
      })}
    >
      <List>
        <ListItem button={true}>
          <ListItemIcon>
            <DateIcon />
          </ListItemIcon>
          <ListItemText primary="Order by date" onClick={this.handleOrderByDate}/>
        </ListItem>
        <ListItem button={true}>
          <ListItemIcon>
            <CurrencyIcon />
          </ListItemIcon>
          <ListItemText primary="Order by price" onClick={this.handleOrderByPrice}/>
        </ListItem>
      </List>
    </div>);

    return (
      <>
      <Hidden mdUp={true}>
      <Drawer
        variant="temporary"
        anchor='left'
        open={this.props.layout.sidePanelStatus}
        onClose={this.handleClose}
        className={mainDrawer}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
      >
        <div
          tabIndex={0}
          role="button"
          onClick={this.handleClose}
          onKeyDown={this.handleClose}
        >
          {list}
        </div>
      </Drawer>
      </Hidden>   
      <Hidden smDown={true} implementation="css">
        <Drawer
          variant="permanent"
          open={true}
          className={mainDrawer}
        >
          {list}
        </Drawer>
      </Hidden>
      </>
    );
  }

  // private handleOpen = () => {
  //   this.props.layout.setSidePanelStatus(true);
  // };

  private handleClose = () => {
    this.props.layout.setSidePanelStatus(false);
  };

  private handleOrderByDate = () => {
    this.props.catalog!.setSearchModifier('startDate.desc');
    this.props.catalog!.getCatalogData();
  }

  private handleOrderByPrice = () => {
    this.props.catalog!.setSearchModifier('salePrice.desc');
    this.props.catalog!.getCatalogData();
  }
}
