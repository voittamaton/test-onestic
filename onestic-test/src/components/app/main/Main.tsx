import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import ArrowDown from "@material-ui/icons/ArrowDownward";
import ArrowUp from "@material-ui/icons/ArrowUpward";
import { observer } from "mobx-react";
import * as React from "react";
import {
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton
} from "../../../../node_modules/@material-ui/core";
import { ICart, ICatalog, ILayout } from "../../../model/rootStore";
import { addShoppingCartIcon, arrowIcon, main } from "./style";

export interface IMain {
  catalog?: ICatalog;
  cart?: ICart;
  layout?: ILayout;
}

@observer
export class Main extends React.Component<IMain> {
  public render() {
    // tslint:disable-next-line:no-console
    console.log(this.props.catalog!.items);
    return (
      <>
        <GridList cellHeight={160} className={main} cols={3}>
          <GridListTile key='arrowup' cols={3}>
            <IconButton className={arrowIcon} onClick={this.handlePreviousPage}>
              <ArrowUp />
            </IconButton>
          </ GridListTile>
          {this.props.catalog!.items.products.map((item: any) => (
            <GridListTile key={item.sku} cols={1}>
              <img src={item.image} alt={item.name} onClick={() => this.handleOpenInfo(item)}/>
              <GridListTileBar
                title={item.name}
                subtitle={<span>${item.salePrice}</span>}
                actionIcon={
                  <>
                    <IconButton className={addShoppingCartIcon} onClick={() => this.handleAddToCart(item)}>
                      <AddShoppingCartIcon />
                    </IconButton>
                  </>
                }
              />
            </GridListTile>
          ))}
          <GridListTile key='arrowdown' cols={3}>
            <IconButton className={arrowIcon} onClick={this.handleNextPage}>
              <ArrowDown />
            </IconButton>
          </ GridListTile>
        </GridList>
      </>
    );
  }

  private handleOpenInfo = (item: any) => {
    this.props.layout!.setSelectedItem(item);
    this.props.layout!.setModalStatus(true);
  }

  private handlePreviousPage = () => {
    const currentPage: number = this.props.catalog!.currentPage;
    if (currentPage > 1) {
      this.props.catalog!.setCurrentPage(currentPage - 1);
      this.props.catalog!.getCatalogData();
    }
  }

  private handleNextPage = () => {
    const currentPage: number = this.props.catalog!.currentPage;
    // tslint:disable-next-line:no-console
    console.log(currentPage);
    
    this.props.catalog!.setCurrentPage(currentPage + 1);
    this.props.catalog!.getCatalogData();
  }

  private handleAddToCart = (item: any) => {
    this.props.cart!.addProduct(item);
  }
}
