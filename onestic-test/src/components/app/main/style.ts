import { media, style } from "typestyle/lib";

export const main = style({
    flexGrow: 1,
    paddingBottom: '100px',
    paddingLeft: '300px',
    paddingRight: '100px',
    paddingTop: '150px',
}, media({
    maxWidth: 960
}, {
    paddingLeft: 20
}));

export const addShoppingCartIcon = style({
    color: 'white'
});

export const arrowIcon = style({
    color: 'black',
    position: 'relative',
    transform: 'translate(35vw)'
});