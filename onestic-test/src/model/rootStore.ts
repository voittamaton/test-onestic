import { types } from "mobx-state-tree";

export const TLayout = types
  .model({
    modalStatus: types.optional(types.boolean, false),
    selectedItem: types.optional(types.frozen, {
      categoryPath: [],
      title: ''
    }),
    sidePanelStatus: types.optional(types.boolean, false)
  })
  .actions(self => {
    function setSidePanelStatus(newStatus: boolean) {
      self.sidePanelStatus = newStatus;
    }

    function setModalStatus(modalStatus: boolean) {
      self.modalStatus = modalStatus;
    }

    function setSelectedItem(selectedItem: any) {
      self.selectedItem = selectedItem;
    }

    return { setSidePanelStatus, setSelectedItem, setModalStatus };
  });

export const TCatalog = types
  .model({
    currentPage: types.optional(types.number, 1),
    items: types.optional(types.frozen, {
      products: []
    }),
    searchModifier: types.optional(types.string, '')
  })
  .actions(self => {
    function setItems(items: any) {
      self.items = items
    }
    function setCurrentPage(page: number) {
      self.currentPage = page
    }
    function setSearchModifier(modifier: string) {
      self.searchModifier = `&sort=${modifier}`
    }
    return {setCurrentPage, setItems, setSearchModifier};
  })
  .views(self => ({
    getCatalogData() {
      return fetch(`https://api.bestbuy.com/v1/products?pageSize=50&page=${self.currentPage}&apiKey=zkV0vgA2RGeQDBibuvt1Dp5k&format=json${self.searchModifier}`)
      .then((response) => {
        return response.json();
      })
      .then((responseAsJSON) => {
        self.setItems(responseAsJSON);
        // tslint:disable-next-line:no-console
        console.log(self.items);
    });
    }
}));

export const TCartProduct = types
.model({
  amount: types.optional(types.number, 0),
  product: types.frozen,
  sku: types.number
});

export const TCart = types
.model({
  products: types.optional(types.frozen, [])
})
.actions(self => {
  function addProduct(product: ICartProduct) {
    const foundProduct: number = self.products.findIndex((x: ICartProduct) => x.sku === product.sku);
    
    if (foundProduct > -1) {
      self.products[foundProduct].amount = self.products[foundProduct].amount + 1;
    } else {
      self.products = [...self.products, {
        amount: 1,
        product,
        sku: product.sku
      } as ICartProduct]
    }
    // tslint:disable-next-line:no-console
    console.log(self.products);
  }

  function removeProduct(sku: number) {
    const foundProduct: number = self.products.findIndex((x: ICartProduct) => x.sku === sku);

    if (foundProduct > -1) {
    
      if (self.products[foundProduct].amount === 1) {
        self.products.splice(foundProduct, 1);
      } else {
        self.products[foundProduct].amount = self.products[foundProduct].amount - 1;
      }
    }
  }

  return {addProduct, removeProduct};
});


export const TRootModel = types.model({
  cart: TCart,
  catalog: TCatalog,
  layout: TLayout
});

export const rootModel = TRootModel.create({
  cart: {},
  catalog: {},
  layout: {}
});

export type ILayout = typeof TLayout.Type;
export type ICatalog = typeof TCatalog.Type;
export type IRootModel = typeof TRootModel.Type;
export type ICartProduct = typeof TCartProduct.Type;
export type ICart = typeof TCart.Type;

