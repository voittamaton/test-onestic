# Creación de un FrontStore

Realiza una aplicación web SPA, utilizando HTML5, SCSS, y JavaScript (puedes usar Angular/React/Vue como framework), que muestre una parrilla de productos a partir de un endpoint de BestBuy y, a través de una ficha de producto, mostrar una información más extendida del producto junto con una galería de imágenes.
Utiliza Material Design para los estilos y componentes de la aplicación.

El endpoint de BestBuy es:
https://api.bestbuy.com/v1/products?pageSize=50&page=1&apiKey=zkV0vgA2RGeQDBibuvt1Dp5k&format=json

Tienes documentación de la API de BestBuy aquí:
https://bestbuyapis.github.io/api-documentation/#user-guide

## Requisitos de la Aplicación:

### 1. Layout
- La cabecera dispondrá de un botón de menú a la izquierda, logotipo de Onestic (incluido en este repositorio), y a la derecha llevará un botón de carrito y otro de usuario. Debe estar siempre visible en la parte superior de todas las páginas de la aplicación. Los iconos se utilizarán de la librería de Material Design
- Usar los colores corporativos para toda la aplicación
- Debe ser Mobile First. Para desktop, la página estará en un contenedor de máximo 1200px
- Se utilizará SCSS para los estilos

### 2. Grid de productos
- Estará dividido en 2 columnas
- Dispondrá de un sidebar en el lateral izquierdo (en mobile será un bloque colapsado antes de los productos) que contendrá filtros básicos: ordenar (por fecha y por precio) y rango de precio
- El grid de los productos estará en la columna derecha y será de 3 por fila en el caso de desktop, con una paginación de 50 productos por página.
- Cada item del grid tendrá: foto en miniatura, nombre del producto, precio, y botón de añadir al carrito. El nombre del producto será el enlace para ver la ficha del mismo.
- Habrá un paginador al principio y al final del grid para avanzar o retroceder página, si procede.

### 3. Ficha de producto
- Tendrá un breadcrumb con las categorías
- Estará dividida en 2 partes: una principal donde se mostrará una imagen del producto junto con sus miniaturas, en la parte izquierda, y descripción básica del producto en el lado derecho, así como su precio y botón de añadir al carrito; y otra que indicará la descripción extendida. Sé creativo en el UX.

### 4. Añadir al carrito
- Los botones de añadir al carrito simplemente añadirán el producto a una lista que se podrá visualizar pulsando en el icono del carrito.
- Esta lista será un drawer layout que aparecerá desde la derecha y desplazará todo el layout hacia la izquierda
- Tendrá un botón de confirmar pedido que no será funcional
- Deberá totalizar y permitir aumentar o disminuir unidades, así como eliminar

### 5. Autenticación
- Pulsando sobre el icono de usuario de la cabecera, se desplegará un drawer layout desde la derecha para introducir correo electrónico y contraseña y un botón de enviar
- La autenticación se realizará sobre el fichero customers.json añadido en este repositorio
- Una vez realice el acceso, se le mostrará una alerta al usuario de que ya está auntetificado
- Si se pulsa sobre el icono de usuario estando ya logueado, en el drawer layout se mostrarán enlaces ficticios y cerrar sesión para desconectarse
- La sesión debe ser persistente y tener una expiración de 1 hora

## Objetivo de la prueba
El objetivo de la prueba es demostrar las aptitudes que buscamos para este perfil. Valoraremos positivamente:

- Código limpio, sencillo, y mantenible

- Conocimiento del framework usado y del lenguaje utilizado

- UI/UX empleado

- Patrones de desarrollo

- Arquitectura de la prueba

- Agilidad en la resolución

- Gitflow usado

## Instrucciones
Para realizar la prueba:

1. Crea un fork de este repositorio en tu cuenta de BitBucket.

2. Escribe tu código usando tu flow habitual en git

3. Realiza un Pull Request para que podamos revisar tu resolución.